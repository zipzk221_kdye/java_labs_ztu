package com.education.ztu;

import com.education.ztu.Product;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main4 {
    public static void main(String[] args) {
        Product p1 = new Product("Milk", 4),
                p2 = new Product("Chips", 3),
                p3 = new Product("Pineapple", 2),
                p4 = new Product("Apple", 200),
                p5 = new Product("Taxi", 100),
                p6 = new Product("Kek", 155);

        HashMap<Integer, Product> productHashMap = new HashMap<Integer, Product>(),
                anotherHashMap = new HashMap<Integer, Product>();
        productHashMap.put(p1.hashCode(), p1);
        productHashMap.put(p2.hashCode(), p2);
        productHashMap.put(p3.hashCode(), p3);
        productHashMap.put(p4.hashCode(), p4);
        productHashMap.put(p5.hashCode(), p5);

        System.out.printf("Get milk: %s\n", productHashMap.get(p1.hashCode()));
        System.out.printf("Are key [%s] contains:   %s\n",p6, productHashMap.containsKey(p6.hashCode()));
        System.out.printf("Are value [%s] contains: %s\n", p2, productHashMap.containsValue(p2));

        anotherHashMap.putIfAbsent(p6.hashCode(), p6);
        anotherHashMap.putIfAbsent(p6.hashCode(), p2);
        anotherHashMap.putIfAbsent(p6.hashCode(), p3);

        System.out.printf("Before\nKeyset: %s\n", productHashMap.keySet());
        System.out.printf("Values: %s\n", productHashMap.values());

        productHashMap.putAll(anotherHashMap);
        System.out.printf("After putAll\nKeyset: %s\n", productHashMap.keySet());
        System.out.printf("Values: %s\n", productHashMap.values());

        productHashMap.remove(p4.hashCode(), p4);
        System.out.printf("After remove p4\nKeyset: %s\n", productHashMap.keySet());
        System.out.printf("Values: %s\n", productHashMap.values());

        System.out.printf("Size of map: %s\n",productHashMap.size());

        Set<Map.Entry<Integer,Product>> set = productHashMap.entrySet();
        System.out.println(set);
        // that type have no method like
        // getKey(?)
        // setKey(?)
        // setValue(?)
    }
}
