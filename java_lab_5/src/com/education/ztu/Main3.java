package com.education.ztu;
import com.education.ztu.Product;
import java.util.TreeSet;

public class Main3 {
    public static void main(String[] args) {
        // Task 5
        TreeSet<Product> productTree = new TreeSet<>();
        Product p1 = new Product("Milk", 4),
                p2 = new Product("Chips", 3),
                p3 = new Product("Pineapple", 2),
                p4 = new Product("Apple", 200),
                p5 = new Product("Taxi", 100),
                p6 = new Product("Kek", 155);

        productTree.add(p1);
        productTree.add(p2);
        productTree.add(p3);
        productTree.add(p4);
        productTree.add(p5);

        System.out.printf("First: %s\n", productTree.first());
        System.out.printf("Last: %s\n", productTree.last());
        System.out.printf("HeadSet for %s \n%s\n\n", p3, productTree.headSet(p3));

        System.out.printf("SubSet from [%s] to [%s]\n%s\n\n", p1, p4, productTree.subSet(p1, p4));
        System.out.printf("TailSet for %s\n%s\n\n", p1, productTree.tailSet(p1));
        System.out.printf("Ceiling for %s\n%s\n\n", p2, productTree.ceiling(new Product("Kek", 155)));
        System.out.printf("Floor is: %s\n\n", productTree.floor(p6));
        System.out.printf("Higher than %s is: \n%s\n\n", p6, productTree.higher(p6));
        System.out.printf("Lower than %s is: \n%s\n\n", p6, productTree.lower(p6));

        System.out.printf("pollFirst: %s\n\n", productTree.pollFirst());
        System.out.printf("pollLast: %s\n\n", productTree.pollLast());


        System.out.println(productTree);
        System.out.println(productTree.descendingSet());
    }
}
