package com.education.ztu;

import java.util.Comparator;
import java.util.Objects;

public class Product implements Comparable<Product>, Cloneable{
    private String name;
    private int price;

    public Product (String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        return String.format("[%s: %d]", name, price);
    }

    @Override
    public final boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(!(o instanceof Product)) {
            return false;
        }
        Product ofter = (Product) o;
        return this.price == ofter.price && this.name.equals(ofter.name);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.name, this.price);
    }


    @Override
    public int compareTo(Product product) {
        if(this.price == product.price) {
            return this.name.compareTo(product.name);
        }
        return Integer.compare(this.price, product.price);
    }
}
