package com.education.ztu;
import com.education.ztu.Product;
import java.util.ArrayDeque;

public class Main2 {
    public static void main(String[] args) {
        ArrayDeque<Product> products = new ArrayDeque<>();

        Product p1 = new Product("Milk", 4),
                p2 = new Product("Chips", 3),
                p3 = new Product("Pineapple", 2),
                p4 = new Product("Apple", 200),
                p5 = new Product("Taxi", 100);

        products.push(p1);
        products.push(p2);
        products.push(p3);
        products.push(p4);

        System.out.printf("Before manipulations: \n%s\n", products);

        System.out.printf("Offer last Taxi: %s\n", products.offerLast(p5));
        System.out.printf("Get last: %s\n", products.getLast());
        System.out.printf("Get first: %s\n", products.getFirst());
        System.out.printf("Peek last: %s\n", products.peekLast());
        System.out.printf("Remove last: %s\n", products.removeLast());
        System.out.printf("Peek last: %s\n", products.peekLast());

        System.out.printf("Pop method: %s\n", products.pop());
        System.out.printf("Pop method: %s\n", products.pop());

        System.out.printf("After manipulations: \n%s\n", products);
    }

}
