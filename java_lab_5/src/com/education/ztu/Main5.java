package com.education.ztu;

import com.education.ztu.Product;

import java.util.*;

public class Main5 {
    public static void main(String[] args) {
        Product p1 = new Product("Milk", 4),
                p2 = new Product("Chips", 3),
                p3 = new Product("Pineapple", 2),
                p4 = new Product("Apple", 200),
                p5 = new Product("Taxi", 100);

        Product[] products_array = new Product[]{p1, p2, p3, p4, p5};
        List<Product> productList = Arrays.asList(products_array);

        Comparator<Product> by_price = Comparator.comparing(Product::getPrice);
        System.out.printf("Simple array:\n%s\n\n", productList);

        productList.sort(by_price);
        System.out.printf("Sorted array:\n%s\n\n", productList);

        Collections.reverse(productList);
        System.out.printf("Reversed array:\n%s\n\n", productList);

        Collections.shuffle(productList);
        System.out.printf("Shuffle array:\n%s\n\n", productList);

        System.out.printf("Collection max: %s\n", Collections.max(productList));
        System.out.printf("Collection min: %s\n\n", Collections.min(productList));

        Collections.rotate(productList, 2);
        System.out.printf("Rotate array:\n%s\n\n", productList);

        Collections.fill(productList, p5);
        System.out.printf("Fill array:\n%s\n\n", productList);

        System.out.printf("Freq %s:\n%s\n\n", p5, Collections.frequency(productList, p5));
        System.out.printf("Checked:\n%s\n\n", Collections.checkedCollection(productList, Product.class));
    }
}
