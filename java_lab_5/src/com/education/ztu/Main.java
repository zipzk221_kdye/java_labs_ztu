package com.education.ztu;

import com.education.ztu.Product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Task 1
        List<Product> products = new ArrayList<>(), list_today = new ArrayList<>();

        Product p1 = new Product("Milk", 4),
                p2 = new Product("Chips", 3),
                p3 = new Product("Pineapple", 2),
                p4 = new Product("Apple", 200),
                p5 = new Product("Taxi", 100);

        list_today.add(p1);
        list_today.add(p2);
        list_today.add(p2);
        list_today.add(p3);
        list_today.add(p5);

        products.addAll(list_today);
        System.out.printf("get[0] = %s\n", products.get(0));
        System.out.printf("IndexOf: %s\n", products.indexOf(p3));
        System.out.printf("LastIndexOf: %s\n", products.lastIndexOf(p2));

        System.out.println("Display via iterator");
        Iterator<Product> i1 = products.iterator();
        while(i1.hasNext()) {
            System.out.printf("%s\n",i1.next());
        }

        System.out.println("Remove product[2]");
        products.remove(2);

        System.out.println("Display via listIterator");
        for(Iterator<Product> i2 = products.listIterator(); i2.hasNext(); ) {
            System.out.printf("%s\n", i2.next());
        }

        products.set(1, p4);
        System.out.printf("Set new product[1]: %s\n", products.get(1));

        Comparator<Product> nameComporator = Comparator.comparing(Product::getName);
        products.sort(nameComporator);
        System.out.println("Sort by name");
        display_list(products);

        System.out.println("Display first part of sublist");
        display_list(products.subList(0, products.size()/2));


        System.out.printf("Are list contain Apple: %s\n", products.contains(p4));
        System.out.printf("Retail all from product\n");
        products.retainAll(list_today);
        display_list(products);

        System.out.printf("List class is:       %s\n", products.getClass());
        System.out.printf("Before toArray():    %s\n", products.toArray().getClass());

        System.out.printf("List[%d] before clear - isEmpty: %s\n", list_today.size(), list_today.isEmpty());
        list_today.clear();
        System.out.printf("List[%d] After clear - isEmpty: %s\n",list_today.size(), list_today.isEmpty());
    }

    public static void display_list(List<Product> list) {
        for (Product product : list) {
            System.out.printf("%s\n", product);
        }
    }
}