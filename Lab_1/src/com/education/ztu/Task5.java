package com.education.ztu;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean pass=true;
        int num=0;
        while(pass){
            System.out.print("Write number (from 10 to 99): ");
            num = scan.nextInt();
            if(num>=10 && num<=99)
                pass=false;
            else {
                System.out.print("Wrong number!\n");
                pass=true;
            }
        }


        int num1 = num/10;
        int num2 = num%10;
        int result =num1+num2;

        System.out.print(result);
    }
}
