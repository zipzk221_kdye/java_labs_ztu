package com.education.ztu;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Write first number: ");
        int num1 = scan.nextInt();
        System.out.print("Write second number: ");
        int num2 = scan.nextInt();
         int result = num1+num2;
        System.out.printf("Sum = %d", result);
        scan.close();
    }
}
