package com.education.ztu;
import java.util.Scanner;

public class Task6 {
    static Integer[] fibonacci(int number) {
        Integer[] result = new Integer[number];
        result[0]=1;
        result[1]=1;
        for(int i=2;i<number;i++){
            result[i]=result[i-1]+result[i-2];
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("How long should the array be: ");
        int n = scan.nextInt();
        Integer[] smth = fibonacci(n);
        for(int i=0;i<smth.length;i++){
            System.out.print(smth[i]+" ");
        }
    }
}
