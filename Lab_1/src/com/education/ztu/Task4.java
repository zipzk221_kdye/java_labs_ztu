package com.education.ztu;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Write first number: ");
        int num1 = scan.nextInt();
        System.out.print("Write second number: ");
        int num2 = scan.nextInt();

        int max = Math.max(num1,num2);
        int result=0;
        for(;max>=0;max--){
            if(num1%max==0 && num2%max==0){
                result = max;
                break;
            }
        }
        System.out.print(result);
    }
}
