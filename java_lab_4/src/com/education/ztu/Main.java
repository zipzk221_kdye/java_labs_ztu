package com.education.ztu;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.Formatter;

public class Main {
    public static void first(String str){
        System.out.println(str.toCharArray()[str.length()-1]);
        if (str.endsWith("!!!"))
            System.out.println("Строка заканчиваеться на '!!!'");
        if (str.startsWith("I learn"))
            System.out.println("Строка начинаеться на 'I learn'");
        if (str.contains("Java"))
            System.out.println("Строка имеет 'Java'");
        System.out.println("Позиция 'Java' = "+str.lastIndexOf("Java"));
        System.out.println(str.replace("a","o"));
        System.out.println(str.toUpperCase());
        System.out.println(str.toLowerCase());
        System.out.println(str.substring(8,12));
    };

    public static void check(){
        Formatter form = new Formatter();
        form.format("Date and time of purchase: %25s%n", "16.11.2022 17:20:51");
        form.format("====================================================%n");
        form.format("%-3s %-15s %-15s %10s%n","№","Product","Category","Price");
        form.format("====================================================%n");
        form.format("%-3s %-15s %-15s %12.2f UAH%n","1.","Jeans","Woman wear",1500.23);
        form.format("%-3s %-15s %-15s %12.2f UAH%n","2.","Skirt","Woman wear",900.83);
        form.format("%-3s %-15s %-15s %12.2f UAH%n","3.","Shirt","Man wear",1400.78);
        form.format("%-3s %-15s %-15s %12.2f UAH%n","4.","Hoodie","Man wear",1800.12);
        form.format("%-3s %-15s %-15s %12.2f UAH%n","5.","Blouse","Woman wear",1300.79);
        form.format("====================================================%n");
        form.format("All cost: %38s UAH",6900.79);
        System.out.println(form);
    };

    public static boolean checkCredentials(String login,String password,String confirmPassword){
        if(!(login.contains("q")||login.contains("w")||login.contains("e")||login.contains("r")||login.contains("t")||login.contains("y")||login.contains("u")||login.contains("i")||login.contains("o")||login.contains("p")||login.contains("a")||login.contains("s")||login.contains("d")||login.contains("f")||login.contains("g")||login.contains("h")||login.contains("j")||login.contains("k")||login.contains("l")||login.contains("z")||login.contains("x")||login.contains("c")||login.contains("v")||login.contains("b")||login.contains("n")||login.contains("m")||login.contains("Q")||login.contains("W")||login.contains("E")||login.contains("R")||login.contains("T")||login.contains("Y")||login.contains("U")||login.contains("I")||login.contains("O")||login.contains("P")||login.contains("A")||login.contains("S")||login.contains("D")||login.contains("F")||login.contains("G")||login.contains("H")||login.contains("J")||login.contains("K")||login.contains("L")||login.contains("Z")||login.contains("X")||login.contains("C")||login.contains("V")||login.contains("B")||login.contains("N")||login.contains("M")||login.contains("1")||login.contains("2")||login.contains("3")||login.contains("4")||login.contains("5")||login.contains("6")||login.contains("7")||login.contains("8")||login.contains("9")||login.contains("0")||login.contains("_") && login.length()<=20)){
            if(!(password.contains("q")||password.contains("w")||password.contains("e")||password.contains("r")||password.contains("t")||password.contains("y")||password.contains("u")||password.contains("i")||password.contains("o")||password.contains("p")||password.contains("a")||password.contains("s")||password.contains("d")||password.contains("f")||password.contains("g")||password.contains("h")||password.contains("j")||password.contains("k")||password.contains("l")||password.contains("z")||password.contains("x")||password.contains("c")||password.contains("v")||password.contains("b")||password.contains("n")||password.contains("m")||password.contains("Q")||password.contains("W")||password.contains("E")||password.contains("R")||password.contains("T")||password.contains("Y")||password.contains("U")||password.contains("I")||password.contains("O")||password.contains("P")||password.contains("A")||password.contains("S")||password.contains("D")||password.contains("F")||password.contains("G")||password.contains("H")||password.contains("J")||password.contains("K")||password.contains("L")||password.contains("Z")||password.contains("X")||password.contains("C")||password.contains("V")||password.contains("B")||password.contains("N")||password.contains("M")||password.contains("1")||password.contains("2")||password.contains("3")||password.contains("4")||password.contains("5")||password.contains("6")||password.contains("7")||password.contains("8")||password.contains("9")||password.contains("0")||password.contains("_") && password.length()<=20 && password==confirmPassword)){
                System.out.println("Wrong login");
                return false;
            }
            else {
                System.out.println("Wrong login and password");
                return false;
            }
        }
        else {
            System.out.println("Everything okay!");
            System.out.println(login);
            System.out.println(password);
            System.out.println(confirmPassword);
            return true;
        }
    };

    public static void main(String[] args) {
        System.out.println("Task 2");
        System.out.println("\n");
        first("I learn Java!!!");

        System.out.println("\n");
        System.out.println("Task 3");
        System.out.println("\n");
        StringBuilder str = new StringBuilder("4 + 36 = 40");
        System.out.println(str);
        str = new StringBuilder("4 - 36 = -32");
        System.out.println(str);
        str = new StringBuilder("4 * 36 = 144");
        System.out.println(str);

        str = new StringBuilder("Hello");
        str.append(" World!");
        System.out.println(str);

        str = new StringBuilder("4 * 36 = 144");
        str.deleteCharAt(7);
        str.insert(7,"equal");
        System.out.println(str);

        str = new StringBuilder("4 * 36 = 144");
        str.replace(7,8,"equal");
        System.out.println(str);

        str = new StringBuilder("4 * 36 = 144");
        str.reverse();
        System.out.println(str);

        System.out.println("Length - "+str.length());
        System.out.println("Capacity - "+str.capacity());

        System.out.println("\n");
        System.out.println("Task 4");
        System.out.println("\n");
        check();

        System.out.println("\n");
        System.out.println("Task 5");
        System.out.println("\n");
        checkCredentials("login","123","123");

        System.out.println("\n");
        System.out.println("Task 6");
        System.out.println("\n");
        LocalDateTime now = LocalDateTime.of(2022,11,28,16,56,22);
        System.out.println("Час початку 6 завдання: " + now);
        System.out.println("День тижня: " + now.get(ChronoField.DAY_OF_WEEK));
        System.out.println("День року: " + now.get(ChronoField.DAY_OF_YEAR));
        System.out.println("Місяць: " + now.get(ChronoField.MONTH_OF_YEAR));
        System.out.println("Рік: " + now.get(ChronoField.YEAR));
        System.out.println("Години: " + now.get(ChronoField.HOUR_OF_DAY));
        System.out.println("Хвилини: " + now.get(ChronoField.MINUTE_OF_DAY));
        System.out.println("Секунди: " + now.get(ChronoField.SECOND_OF_DAY));
        System.out.println(now.toLocalDate().isLeapYear() ? "Рік високосний " : "Рік не високосний");
        System.out.println("Поточний час: " + LocalDate.now());
        System.out.println("IsAfter: " + LocalDate.now().isAfter(now.toLocalDate()));
        System.out.println("IsBefore: " + LocalDate.now().isBefore(now.toLocalDate()));
        System.out.println("Змінена дата " + now.withMonth(12));
    }


}